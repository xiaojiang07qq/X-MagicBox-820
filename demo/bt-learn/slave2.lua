--- 模块功能：BLE slave示例
-- @author Darren Cheng
-- @module bluetooth.slave
-- @license MIT
-- @copyright openLuat
-- @release 2021.08.24
-- @注意 需要使用core(Luat_VXXXX_RDA8910_BT_FLOAT)版本
module(..., package.seeall)

local useUserService = true -- 用户自定义服务，true启用，false禁用
local function init()
    log.info("bt", "init")
    rtos.on(rtos.MSG_BLUETOOTH, function(msg)
        if msg.event == btcore.MSG_OPEN_CNF then
            sys.publish("BT_OPEN", msg.result) --蓝牙打开成功
        elseif msg.event == btcore.MSG_BLE_CONNECT_IND then
            sys.publish("BT_CONNECT_IND", {["handle"] = msg.handle, ["result"] = msg.result}) --蓝牙连接成功
		elseif msg.event == btcore.MSG_BLE_DISCONNECT_IND then
            log.info("bt", "ble disconnect") --蓝牙断开连接
        elseif msg.event == btcore.MSG_BLE_DATA_IND then
            sys.publish("BT_DATA_IND", {["result"] = msg.result})--接收到的数据内容
        end
    end)
end


sys.taskInit(function()
    sys.wait(5000)
    init() --初始化

    log.info("bt", "poweron")
    btcore.open(0) --打开蓝牙从模式
    sys.waitUntil("BT_OPEN", 5000) --等待蓝牙打开成功

    log.info("bt", "设置蓝牙参数")
    btcore.setname("Cat1BT")-- 设置广播名称
	btcore.setadvparam(0x80,0xa0,0,0,0x07,0) --广播参数设置 (最小广播间隔,最大广播间隔,广播类型,广播本地地址类型,广播channel map,广播过滤策略,定向地址类型,定向地址)
    
    if useUserService then
        log.info("bt", "useUserService")
        btcore.addservice(0xff88)     -- 添加服务uuid
        btcore.addcharacteristic(0xffe1,0x08,0x002)  --添加特征 可写
        -- btcore.addcharacteristic(0xffe1,0x08+0x04,0x002)  --添加特征 可写 特征属性可以写多个,用+连接
        btcore.addcharacteristic(0xffe2,0x10,0x001)  --添加特征 通知
        btcore.adddescriptor(0x2902,0x0001)      --添加描述
        btcore.addcharacteristic(0xffe3,0x02,0x001)  --添加特征 可读
    end

    btcore.advertising(1)-- 打开广播
    _, bt_connect = sys.waitUntil("BT_CONNECT_IND")
    if bt_connect.result ~= 0 then
        return false
    end
    --链接成功
    log.info("bt","connect_handle",bt_connect.handle) -- 连接句柄
    while true do
        _, bt_recv = sys.waitUntil("BT_DATA_IND") -- 等待接收到数据
        local data = ""
        local len = 0
        local uuid = ""
        while true do
            local recvuuid, recvdata, recvlen = btcore.recv(3)
            if recvlen == 0 then
                break
            end
            uuid = recvuuid
            len = len + recvlen
            data = data .. recvdata
        end
        if len ~= 0 then
            log.info("bt","recv_data:", data)
            log.info("bt","recv_data_len", len)
            log.info("bt","recv_uuid:", string.toHex(uuid))
            if data == "close" then
                btcore.disconnect()--主动断开连接
            end
            if useUserService then
                btcore.send(data, 0xffe2, bt_connect.handle)    -- 发送数据(数据 对应特征uuid 连接句柄)
            end
            btcore.send(data, 0xfee2, bt_connect.handle)    -- 发送数据(数据 对应特征uuid 连接句柄)
        end
    end
end)