--- 模块功能：蓝牙功能测试
-- @author openLuat
-- @module bluetooth.beacon
-- @license MIT
-- @copyright openLuat
-- @release 2020.09.27
-- @注意 需要使用core(Luat_VXXXX_RDA8910_BT_FLOAT)版本
module(..., package.seeall)

local function init()
    log.info("bt", "init")
    rtos.on(rtos.MSG_BLUETOOTH, function(msg)
        if msg.event == btcore.MSG_OPEN_CNF then
            sys.publish("BT_OPEN", msg.result) --蓝牙打开成功
        end
    end)
end


sys.taskInit(function()
    sys.wait(5000)

    init() -- 初始化

    log.info("bt", "poweron")
    btcore.open(0) --打开蓝牙从模式
    sys.waitUntil("BT_OPEN", 5000) --等待蓝牙打开成功

    log.info("bt", "设置蓝牙参数")
    btcore.setadvparam(0x80,0xa0,0,0,0x01,0) --广播参数设置 (最小广播间隔,最大广播间隔,广播类型,广播本地地址类型,广播channel map,广播过滤策略,定向地址类型,定向地址)
    btcore.setbeacondata("AB8190D5D11E4941ACC442F30510B4AB",10107,50179) --beacon设置  (uuid,major,minor)
    
    btcore.advertising(1)-- 打开广播
end)



