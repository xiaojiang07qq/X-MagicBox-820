--- 模块功能：BLE广播示例
-- @author Darren Cheng
-- @module bluetooth.slave
-- @license MIT
-- @copyright openLuat
-- @release 2021.08.24
-- @注意 需要使用core(Luat_VXXXX_RDA8910_BT_FLOAT)版本
module(..., package.seeall)

local function init()
    log.info("bt", "init")
    rtos.on(rtos.MSG_BLUETOOTH, function(msg)
        if msg.event == btcore.MSG_OPEN_CNF then
            sys.publish("BT_OPEN", msg.result) --蓝牙打开成功
        end
    end)
end


sys.taskInit(function()
    sys.wait(5000)

    init() --初始化
    log.info("bt", "开蓝牙")
    btcore.open(0)  --打开蓝牙从模式
    sys.waitUntil("BT_OPEN", 5000)  --等待蓝牙打开成功

    log.info("bt", "设置蓝牙参数")
    btcore.setname("Cat1BT") -- 设置广播名称

    ------------ 设置蓝牙广播数据（LTV格式） --------------
    advData = "64617272656e"
    advType = "08"
    advLenth = string.format("%02x",(advData:len()/2)+1)
    btcore.setadvdata(string.fromHex(advLenth .. advType .. advData))

    ------------ 设置蓝牙响应包数据（LTV格式） --------------
    rspData = "6368656e67"
    rspType = "08"
    rspLenth = string.format("%02x",(rspData:len()/2)+1)
    btcore.setscanrspdata(string.fromHex(rspLenth .. rspType .. rspData))

    btcore.setadvparam(0x80,0xa0,0,0,0x07,0)    --广播参数设置 (最小广播间隔,最大广播间隔,广播类型,广播本地地址类型,广播channel map,广播过滤策略,定向地址类型,定向地址)
    btcore.advertising(1)   -- 打开广播
end)