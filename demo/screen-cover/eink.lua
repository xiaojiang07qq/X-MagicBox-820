module(...,package.seeall)

local es = require"epd1in54_GDEH0154D67"

sys.subscribe("KEY_EVENT", function(k,e)
    if k == "C" and e then
        collectgarbage("collect")
        local scr = disp.getframe()
        local eData = {}
        for y=1,200 do
            for x=1,25 do
                local now = x + (y-1) * 25
                eData[now] = 0
                local p
                for i=1,8 do
                    p = i+(x-1)*8+(y+19)*240+20
                    if  scr:byte(p*2) == 0 and
                        scr:byte(p*2-1) == 0 then
                        --eData[now] = eData[now] + bit.lshift(1,8-i)
                    else
                        eData[now] = eData[now] + bit.lshift(1,8-i)
                    end
                end
            end
        end
        scr = nil
        collectgarbage("collect")
        sys.taskInit(function ()
            es.init()
            es.showPictureN(eData)
            es.deepSleep()
            collectgarbage("collect")
        end)
    end
end)





