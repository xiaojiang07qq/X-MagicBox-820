module(...,package.seeall)
require"record"

--上下对称模式？
local same = false
local temp = {}

function update()
    disp.clear()
    disp.setfontheight(32)
    lcd.CHAR_WIDTH = 16
    lcd.putStringCenter("环境音量",120,lcd.gety(0),255,100,100)
    disp.setfontheight(16)
    lcd.CHAR_WIDTH = 8
    if #temp == 240 then
        local td,tm
        for i=1,#temp do
            if same then
                td = 120+temp[i]
                if td > 239 then td = 239 end
                if td < 0 then td = 0 end
                --disp.drawrect(i-1,td,i-1,td,0xffff)
                if td >= 120 then
                    disp.drawrect(i-1,120,i-1,td,0xffff)
                else
                    disp.drawrect(i-1,td,i-1,120,0xffff)
                end
            else
                tm = 120+math.abs(temp[i])
                td = 120-math.abs(temp[i])
                if tm > 239 then tm = 239 end
                if td < 0 then td = 0 end
                disp.drawrect(i-1,td,i-1,tm,0xffff)
            end
        end
    end
end

local keyEvents = {
    ["3"] = function ()
        same = not same
    end,
}
keyEvents["A"] = keyEvents["3"]
keyEvents["OK"] = keyEvents["3"]

function key(k,e)
    if not e then return end
    if keyEvents[k] then
        keyEvents[k]()
        page.update()
    end
end

function close()
    record.stop()
end


function open()
    record.start(0x7FFFFFFF,function (result,size,tag)
        if result and tag=="STREAM" then
            local s = audiocore.streamrecordread(size)
            if #s < 480 then return end
            temp = {}
            for i=1,240 do
                local d = s:byte(i*2)--低八位数据用不上。。
                if d > 127 then d = d - 256 end
                table.insert(temp,d)
            end
            page.update()
        end
    end,"STREAM",0,audiocore.PCM,1000)
end
