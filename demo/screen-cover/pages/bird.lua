module(...,package.seeall)
require"tools"
require"audio"

local timer_id

local w,h
local bird
local bird_speed
local pipe
local pu,pd
local pw
local ph
local death
local lastKey
local score

local dt = 1
function update()
    if not death then--没死
        --该刷新水管了
        if pipe < -pw then
            pipe = w
            --假设中间总是留出200的空间，roll一个空间的y数据出来
            local y = tools.random() % (h-ph)
            pu = y
            pd = h-y-ph
        end
        --如果按按鼠标了，鸟速度马上变成向上的（必须上次检查的时候鼠标没安下，不然会一直加）
        if lastKey then
            bird_speed = -15
            lastKey = nil
        end
        --管子往左移
        pipe = pipe - dt * 8
        if pipe == 64 then--正好过去了
            --播放声音会卡一下，先注释掉
            --audio.play(0, "FILE", "/lua/pass.mp3", nvm.get("vol"))
            score = score + 1
        end
    end
    if not death or (death and bird < h - 10) then
        bird_speed = bird_speed + dt * 2
        --鸟往下掉
        bird = bird + bird_speed * dt
    end
    if death and bird > h - 10 then
        bird = h - 10
    end

    if not death and(
        ((pipe > w/2-10-pw and pipe < w/2+10) and (bird-10 < pu or bird+10 > h-pd))--检查是不是撞到管子了
        or (bird+10 > h or bird-10 < 0))--检查是不是到天上或者地上了
        then
        death = true--记一下，死了
        audio.play(0, "FILE", "/lua/shot.mp3", nvm.get("vol"))
    end

    disp.clear()
    disp.putimage("/lua/bg.png")

    --画上管子
    if pu and pd then
        local x,y,xx,yy = 0,0,pw,h
        if pipe < 0 then x = -pipe end
        if pipe+pw > w then xx = pw - ((pipe+pw) - w) end
        y = h-pu
        disp.putimage("/lua/pu.png",tools.lineAdd(pipe,w,0),0,-1,x,y,xx,yy)
        local x,xx = 0,pw-1
        if pipe < 0 then x = -pipe end
        if pipe+pw >= w then xx = pw - ((pipe+pw) - w) end
        disp.putimage("/lua/pd.png",tools.lineAdd(pipe,w,0),h-pd,-1,x,0,xx,pd-1)
    end
    --画鸟
    local y,yy = 0,19
    if bird-10 < 0 then y = -(bird-10) end
    if bird+10 >= h then yy = 19-(bird+10-240) end
    disp.putimage("/lua/bird"..(bird%3)..".png",w/2-10,bird-10,-1,0,y,19,yy)

    lcd.text(tostring(score),5,0,0,0,0)
end

function key(k,e)
    if not e then return end
    if k == "A" or k == "3" then
        lastKey = true
    end
end

function open()
    w,h = 240,240
    bird = h/2 --鸟当前的高
    bird_speed = 0--小鸟速
    pipe = -100 --水管当前
    pu,pd = nil,nil--上下水
    pw = 60--水管的宽度
    ph = 100 --水管中间留的空间
    death = nil--是不是死了
    lastKey = nil
    score = 0 --分数
    timer_id = sys.timerLoopStart(page.update,5)
end

function close()
    if timer_id then
        sys.timerStop(timer_id)
        timer_id = nil
    end
end
