module(...)

--保存日志到tf卡
--saveLog = true

--音量
vol = 4

--开启卫星图
map = false

--地图图片列表
-- maps = {
--     {
--         ts = 123456,--时间戳
--         lat = "1.234",
--         lng = "1.234",
--     },
--     ...
-- }
maps = {}

--定位坐标列表
--暂定最大50个
locs = {

}

--控制屏幕方向
--用盖板要用60 直接接屏用00
screen = 0
xoffset = 0
yoffset = 0

--出厂测试通过？
--用户到手里的都是通过的
--仅供出厂测试用
test = true
simTested = 0
--simccid = "xxxxx"
