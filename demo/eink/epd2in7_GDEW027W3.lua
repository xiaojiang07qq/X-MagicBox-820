module(...,package.seeall)

require"misc"
require"utils"
require"pins"

--电压域
pmd.ldoset(15,pmd.LDO_VLCD)

log.info("spi.setup",spi.setup(spi.SPI_1,0,0,8,13000000,0,0))

local function getBusyFnc(msg)
    log.info("Gpio.getBusyFnc",msg)
    if msg==cpu.INT_GPIO_POSEDGE then--上升沿中断
        --不动作
    else--下降沿中断
        sys.publish("BUSY_DOWN")
    end
end

--初始化三个控制引脚
local getBusy         = pins.setup(7,getBusyFnc)
local setRST          = pins.setup(12,1)
local setDC           = pins.setup(18,1)

-- Display resolution
EPD_WIDTH       = 264
EPD_HEIGHT      = 176

local function wait()
    while getBusy() == 1 do  -- 0: idle, 1: busy
        sys.waitUntil("BUSY_DOWN",5000)
    end
end

setDC(1)
local function sendCommand(data)
    --log.info("epd2in7.sendCommand",data)
    setDC(0)
    spi.send(spi.SPI_1,string.char(data))
end

local function sendData(data)
    --log.info("epd2in7.sendData",data)
    setDC(1)
    spi.send(spi.SPI_1,string.char(data))
end

local function sendDataString(data)
    --log.info("epd2in7.sendData",data)
    setDC(1)
    spi.send(spi.SPI_1,data)
end

local function sendDataTrans(data)
    setDC(1)
    local temp
    for i=EPD_WIDTH,1,-1 do
        for j=EPD_HEIGHT/8,1,-1 do
            temp = (j-1)*EPD_WIDTH+i
            spi.send(spi.SPI_1,data:sub(temp,temp))
        end
    end
end

local function reset()
    log.info("epd2in7.reset","")
    setRST(1)
    sys.wait(20)
    setRST(0)
    sys.wait(5)
    setRST(1)
    sys.wait(20)
end

function deepSleep()
    log.info("epd2in7.deepSleep","")
    sendCommand(0X02) --power off
    sendCommand(0X07) --deep sleep
    sendData(0xA5)
    setDC(0)
end

function init()
    log.info("epd2in7.init","")
    reset()

    sendCommand(0x06)-- --boost soft start
    sendData(0x07)--A
    sendData(0x07)--B
    sendData(0x17)--C
    sys.wait(1000)
    sendCommand(0x04)
    wait()
    sendCommand(0x00); --panel setting
    sendData(0x1B); --LUT from OTP，128x296
    sendCommand(0x16);
    sendData(0x00); --KW-BF   KWR-AF	BWROTP 0f
    sendCommand(0xF8); --boost设定
    sendData(0x60);
    sendData(0xa5);
    sendCommand(0xF8); --boost设定
    sendData(0x73);
    sendData(0x23);
    sendCommand(0xF8); --/boost设定
    sendData(0x7C);
    sendData(0x00);
    sendCommand(0X50);
    sendData(0x97); -- WBmode

    wait()
    log.info("epd2in7.init","done")
end

-- 填充数据函数
local function Fillter()
    sendCommand(0x10)--Transfer old data
    for i = 1, 5808 do
        sendData(0xFF)
    end
end

function display_frame()
    log.info("epd2in7.display_frame","start")
    sendCommand(0x12)
    sys.wait(5)
    wait()
    log.info("epd2in7.display_frame","done")
end

function clear(color)
    Fillter()
    sendCommand(0x13)
    for i = 1, 5808 do
        sendData(color)
    end
    display_frame()
    log.info("epd2in7.clear","done")
end

--输入值：数组
function showPictureN(pic)
    wait()
    log.info("epd2in7.showPicture","")
    Fillter()
    sendCommand(0x13)
    for i=1,#pic do
        sendData(pic[i])
    end
    display_frame()
end

--输入值：string
function showPicture(pic)
    wait()
    log.info("epd2in7.showPicture","")
    Fillter()
    sendCommand(0x13)
    sendDataString(pic)
    display_frame()
end

--输入值：string,按页显示的数据
function showPicturePage(pic)
    wait()
    log.info("epd2in7.showPicturePage","")
    Fillter()
    sendCommand(0x13)
    sendDataTrans(pic)
    display_frame()
end


